import numpy as np

# This file uses following index notation
# c   Axis index (x,y,z)
# o   Symmetry operation index
# g   conjugate class index
# i   irrep index

all_symmetries = ['C1','C2','Oh']

def detect_symmetry(atoms):
    last_sym_ok = None
    for name in all_symmetries:
        symmetry = Symmetry(name, verbose=False)
        ops_sym, ops_total = symmetry.check_symmetry(atoms)
        ok = ops_sym == ops_total
        if ok:
            last_sym_ok = symmetry
        print("%-5s %-3d %-3d %-5s" % (name, ops_sym, ops_total, "OK!" if ok else "No!"))
    if last_sym_ok is not None:
        print("Detected symmetry: %s" % last_sym_ok.name)
    return last_sym_ok
    
def Symmetry(name, verbose=True):
    if name == 'Oh':
        return OhSymmetry(verbose=verbose)
    if name == 'C2':
        return C2Symmetry(verbose=verbose)
    if name == 'C1':
        return C1Symmetry(verbose=verbose)
    from ase import Atoms
    if isinstance(name, Atoms):
        return detect_symmetry(name)
    
    raise ValueError("Unknown symmetry %s" % name)

class SymmetryBaseClass:
    def __init__(self, name, verbose=True):
        self.name = name

        # A set of 3x3 unitary matrices for each op
        self.ops_occ = None 

        # Multiplication table for group
        self.mul_oo = None

        # Inverse table for group
        self.inv_oo = None
        
        # Classwise differentiation of operations
        self.ops_g = None 

        # Classwise differentiation of operations
        self.g_o = None # To which class g an op o belongs to
        
        # Character table
        self.character_ig = None
       
        # Names of the irreducible representations
        self.names_i = None
        
        # Names of symmetry classes
        self.names_g = None

        # Eigenspaces of each irreducible representation
        self.groups_i = None

        self.verbose = verbose

        self._build_ops(name)
        if verbose:
            self.print_ops()
        self._build_multiplication_table()
        self._find_conjugacy_classes()
        self._build_character_table()
        self._detect_conjugacy_classes()
        self._detect_irreps()
        self._name_groups_and_classes()

    def find_vector_permutation(self, R_ac, Rp_ac):
        return np.argmin(np.sum((R_ac[:,None, :] - Rp_ac[None,:,:])**2, axis=2), axis=1)

    def Ni(self):
        return len(self.groups_i)

    def Ng(self):
        return len(self.ops_g)

    def symmetry_projectors(self, permutations_Xo):
        character_ig = self.character_ig
        print(self.character_ig)
        I_XX = np.eye(len(permutations_Xo))
        U_iXx = []
        for i in range(self.Ni()):
            proj_XX = 0
            for g, ops_o in enumerate(self.ops_g):
                chi = character_ig[i, g]
                for o in ops_o:
                    P_XX = I_XX[permutations_Xo[:,o],:] # Create a permutation matrix
                    proj_XX = proj_XX + chi * P_XX.T

            # Find the range of matrix
            eps_n, psi_Xn = np.linalg.eig(proj_XX)
            print(eps_n)
            idx, = np.where(eps_n > 1e-4)
            q, r = np.linalg.qr(psi_Xn[:,idx])
            U_iXx.append(q)
        return U_iXx

    def check_symmetry(self, atoms):
        sym = 0
        ops_occ = self.ops_occ
        R_ac = atoms.get_positions()-atoms.get_center_of_mass()
        P_po = np.zeros( (len(atoms), len(ops_occ)), dtype=int)
        for o, op_cc in enumerate(ops_occ):
            Rp_ac = np.dot(R_ac, op_cc) # Transform atoms according to op o
            P_a = self.find_vector_permutation(R_ac, Rp_ac)
            if np.linalg.norm(Rp_ac[P_a,:] - R_ac[:,:])<1e-8:
                sym += 1
        return sym, len(ops_occ)


    def get_symmetry_permutations(self, atoms):
        ops_occ = self.ops_occ
        R_ac = atoms.get_positions()-atoms.get_center_of_mass()
        P_po = np.zeros( (len(atoms), len(ops_occ)), dtype=int)
        for o, op_cc in enumerate(ops_occ):
            Rp_ac = np.dot(R_ac, op_cc) # Transform atoms according to op o
            P_po[:,o] = self.find_vector_permutation(R_ac, Rp_ac)
        return P_po

    def _build_ops(self, name):
        raise NotImplementedError

    def _name_groups_and_classes(self):
        raise NotImplementedError

    def print_ops(self, ops_occ=None, columns=8):
        if ops_occ is None:
            ops_occ = self.ops_occ
        rows = int(np.ceil(len(ops_occ)/columns))
        for row in range(rows):
            for c in range(3):
                for col in range(columns):
                    idx  = row*columns+col
                    if idx >= len(ops_occ):
                        continue
                    op_cc = ops_occ[idx]
                    print("(%2d %2d %2d)     " % (op_cc[c,0], op_cc[c,1], op_cc[c,2]), end="")
                print()
            print()
        print('Total symmetries: %d' % len(ops_occ))

    def get_op_id(self, op_cc):
        for o1, op1_cc in enumerate(self.ops_occ):
            if np.linalg.norm(op_cc-op1_cc)<1e-8:
                return o1
        raise ValueError("Unknown operation: %s." % str(op_cc))

    def _build_multiplication_table(self):
        ops_occ = self.ops_occ
        N = len(ops_occ)
        mul_oo = np.zeros((N,N), dtype=int)
        inv_oo = np.zeros((N,N), dtype=int)
        for o1, op1_cc in enumerate(ops_occ):
            for o2, op2_cc in enumerate(ops_occ):
                o3 = self.get_op_id(np.dot(op1_cc, op2_cc))
                mul_oo[o1,o2] = o3
                inv_oo[o1,o3] = o2
        self.mul_oo, self.inv_oo = mul_oo, inv_oo

    def print_multiplication_table(self):
        np.set_printoptions(suppress=True, edgeitems=48, linewidth=200)
        print("Multiplication table:")
        print(self.mul_oo)

    def _find_conjugacy_classes(self):
        ops_occ = self.ops_occ
        N = len(ops_occ)
        op_pool = range(N)
        self.ops_g = []

        self.g_o = np.zeros((N,), int) # Conjugacy class index for each op
        while True:
            # Loop until all operations are assigned a class
            if len(op_pool)==0:
                break
            # Take an element from operation pool...
            op1 = op_pool[0]
            op1_cc = ops_occ[op1]
            # ...conjugate it with all possible operations, see the result, and remove any duplicates
            conjugacy_class = np.unique([ self.get_op_id(np.dot(op2_cc, np.dot(op1_cc, op2_cc.T))) for o2, op2_cc in enumerate(ops_occ) ])
        
            # Fill g_o array that maps ops to class
            for op in conjugacy_class:
                self.g_o[op] = len(self.ops_g)
            self.ops_g.append(conjugacy_class)
            # Remove all operations already assigned a class from the pool
            op_pool = list(set(op_pool) - set(conjugacy_class))

        if self.verbose:
            print("Found %d conjugacy classes" % len(self.ops_g))

    def diagonalize_and_group(self, H_oo):
        eps, psi = np.linalg.eigh(H_oo)
        groups = np.unique(abs(eps[:,None]-eps[None,:]) <1e-6, axis=0)
        if self.verbose:
            print("Found %d irreducible representations." % len(groups))

        # Add eigenvales to lists
        groups_i = [ [] for i in range(len(groups)) ]
        for irrep, eig_idx in zip(*np.where(groups)):
            groups_i[irrep].append(psi[:, eig_idx])
        
        return [ np.array(x) for x in groups_i ]

    def _build_character_table(self):
        ops_occ = self.ops_occ
        N = len(ops_occ)

        # Diagonalize arbitrary Hamiltonian (the form 1/(1+g) is irrelevant)
        # to numerically build the character table. The degenerate eigenspaces
        # describe the irreducible representations.
        # There might be an accidental degeneracy, in which case the a representation
        # might end up being a direct product of two representations.
        H_oo = 1/(self.g_o[self.inv_oo]+1)
        groups_i = self.diagonalize_and_group(1/(self.g_o[self.inv_oo]+1))
        self.groups_i = groups_i
        character_ig = np.zeros((len(groups_i), len(self.ops_g)), dtype=int)

        # For each group...
        for i, psi_no in enumerate(groups_i):
            # For each conjugacy class...
            for g, ops in enumerate(self.ops_g):
                # It is not necessary to loop over all group operations. However, it will be a good sanity check.
                # Calculate the trace of this irrep under operation o
                traces_o = np.array( [ np.trace(np.dot(psi_no[:,self.mul_oo[:,o]], psi_no.T.conjugate())) for o in ops ] )
                h = len(psi_no)**0.5
                assert(np.all(abs(traces_o-traces_o[0])<1e-10))
                character_ig[i,g] = int(np.round(traces_o[0]/h))
                
                assert(abs(int(np.round(traces_o[0]))-traces_o[0])<1e-10)

        self.character_ig = character_ig

    def _detect_conjugacy_class(self, ops_occ):
        det_o = np.array( [ np.linalg.det(op_cc) for op_cc in ops_occ ])
        eigs_o = np.array( [ np.sort(np.linalg.eig(op_cc)[0]) for op_cc in ops_occ ])
        if len(det_o) == 1 and np.all(np.isclose(eigs_o, [-1,-1,1])):
            return "C2"
        if len(det_o)==1 and np.all(np.isclose(eigs_o, [-1,-1,-1])):
            return "i" # Inversion flips all axes, -x, -y, -z
        if len(det_o)==1 and np.all(np.isclose(eigs_o, [1, 1, 1])):
            return "E" # Identity leaves all axes intact x, y, z
        if len(det_o)==3 and np.all(np.isclose(eigs_o, [-1,-1,1])):
            return "3C2" # C2 rotation along z is -x, -y, z
        if len(det_o)==6 and np.all(np.isclose(eigs_o, [-1,-1,1])):
            return "6C2"
        if len(det_o)==6 and np.all(np.isclose(eigs_o, [-1,-1j,1j])):
            return "6S4" # -1j and 1j corresponds to 90 rotation. Determinant is -1 thus, improper.
        if len(det_o)==3 and np.all(np.isclose(eigs_o, [-1,1,1])):
            return "3sh" # Horizontal mirror operation flips one of the coordinates
        if len(det_o)==6 and np.all(np.isclose(eigs_o, [-1,1,1])):
            return "6sd"
        if len(det_o)==8 and np.all(np.isclose(eigs_o, [-1, np.exp(-1j*2*np.pi/6), np.exp(1j*2*np.pi/6) ])):
            return "8S6"
        if len(det_o)==6 and np.all(np.isclose(eigs_o, [-1j, 1j, 1 ])):
            return "6C4"
        if len(det_o)==8 and np.all(np.isclose(eigs_o, [np.exp(-1j*np.pi*2/3), np.exp(1j*np.pi*2/3), 1])):
            return "8C3"
        return "bug?"

    def _detect_conjugacy_classes(self):
        self.names_g = [ self._detect_conjugacy_class([ self.ops_occ[o] for o in classops ]) for classops in self.ops_g ]

    def class_id(self, classname):
        try:
            return self.names_g.index(classname)
        except ValueError:
            return None

    def _detect_irreps(self):
        self.names_i = [ self._detect_irrep(self.character_ig[i, :]) for i in range(self.character_ig.shape[0]) ]

    def print_character_table(self, class_order, irrep_order):
        print("%-20s" % "irreps/classes",end="")
        for name in class_order:
            print("%-5s" % name, end="")
        print()
        for iname in irrep_order:
            i = self.names_i.index(iname)
            print("%-20s" % self.names_i[i], end="")
            for gname in class_order:
                g = self.names_g.index(gname)
                print("%-5s" % ("%+02d" % self.character_ig[i,g]), end="")
            print()

class C1Symmetry(SymmetryBaseClass):
    def __init__(self, verbose=True):
        SymmetryBaseClass.__init__(self, 'C1', verbose=verbose)

    def _build_ops(self, name):
        self.ops_occ = [ np.eye(3) ]

    def _detect_irrep(self, signature):
        assert(len(signature)==1 and signature[0] == 1)
        return "A1g"

    def _name_groups_and_classes(self):
        if self.verbose:
            self.print_character_table(class_order = ['E'],
                                       irrep_order = ['A1g'])

class C2Symmetry(SymmetryBaseClass):
    def __init__(self, verbose=True):
        SymmetryBaseClass.__init__(self, 'C2', verbose=verbose)

    def _build_ops(self, name):
        self.ops_occ = [ np.eye(3), np.array([[-1,0,0],[0,-1,0],[0,0,1]]) ] # E and C2

    def _detect_irrep(self, signature):
        if len(signature)==2 and np.all(signature == np.array([1,1])):
            return "A"
        if len(signature)==2 and np.all(signature == np.array([1,-1])):
            return "B"

    def _name_groups_and_classes(self):
        if self.verbose:
            self.print_character_table(class_order = ['E','C2'],
                                       irrep_order = ['A','B'])
          
class OhSymmetry(SymmetryBaseClass):
    def __init__(self, verbose=True):
        SymmetryBaseClass.__init__(self, 'Oh', verbose=verbose)

    def _build_ops(self, name):
        assert(name == 'Oh')
        if self.verbose:
            print('Symmetry operations:')
        def gen_ops(elements=[]):
            if len(elements) < 9:
                for value in [-1, 0, 1]:
                    yield from gen_ops(elements + [ value ])
            else:
                op_cc = np.array(elements).reshape((3,3))
                # Check if unitary
                if np.linalg.norm(np.dot(op_cc.T, op_cc)-np.eye(3))<1e-5:
                    yield op_cc

        self.ops_occ = [ x for x in gen_ops() ]
        assert(len(self.ops_occ) == 48)

    def _find_conjugacy_classes(self):
        SymmetryBaseClass._find_conjugacy_classes(self)
        sizes = np.sort(list(map(len, self.ops_g)))
        assert(np.all(sizes == np.sort([1, 8, 6, 6, 3, 1, 6, 8, 3, 6])))

    def _detect_irrep(self, signature):
        h = signature[ self.class_id("E") ]

        #rotations = [ self.class_id(name) for name in self.names_g if ("C" in name) ]
        rotations = self.class_id("6C2")
        ug = "g" if signature[ self.class_id("i") ] > 0 else "u"
        C = "?"
        N = ""
        if signature[self.class_id("6C4")]>0:
            N = "1"
        else:
            N = "2"
        if h == 1:
            C = "A"
        if h == 2:
            C = "E"
            N = ""
        if h == 3:
            C="T"

        return C+N+ug

    def _name_groups_and_classes(self):
        if self.verbose:
            self.print_character_table(class_order = ['E','8C3','6C2','6C4','3C2','i','6S4','8S6','3sh','6sd'],
                                       irrep_order = ['A1g','A2g','Eg','T1g','T2g','A1u','A2u','Eu','T1u','T2u'])

        #target_ig = np.array([[ 1,  1,  1,  1,  1,  1,  1,  1,  1,  1 ],
        #                      [ 1,  1, -1, -1,  1,  1, -1,  1,  1, -1 ],
        #                      [ 2, -1,  0,  0,  2,  2,  0, -1,  2,  0 ],
        #                      [ 3,  0, -1,  1, -1,  3,  1,  0, -1, -1 ],
        #                      [ 3,  0,  1, -1, -1,  3, -1,  0, -1,  1 ],
        #                      [ 1,  1,  1,  1,  1, -1, -1, -1, -1, -1 ],
        #                      [ 1,  1, -1, -1,  1, -1,  1, -1, -1,  1 ],
        #                      [ 2, -1,  0,  0,  2, -2,  0,  1, -2,  0 ],
        #                      [ 3,  0, -1,  1, -1, -3, -1,  0,  1,  1 ],
        #                      [ 3,  0,  1, -1, -1, -3,  1,  0,  1, -1 ]])
        #target_class_sizes = np.array([ 1, 8, 6, 6, 3, 1, 6, 8, 3, 6 ])


#Symmetry('Oh')
#Symmetry('C2')
