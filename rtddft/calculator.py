import numpy as np

from symmetry import Symmetry

class GSCalculator:
    def __init__(self, atoms, charge=0, symmetry=None):
        self.atoms = atoms
        self.charge = charge
        N = len(atoms)
        self.N = N

        if symmetry is None:
            symmetry = Symmetry(atoms)
        self.symmetry = symmetry

        self.permutations_ao = self.symmetry.get_symmetry_permutations(atoms)
        self.U_iXx = symmetry.symmetry_projectors(self.permutations_ao)
        # Number of irreps
        Ni = symmetry.Ni()
        self.Ni = Ni

        assert( (len(atoms)-charge) % 2 == 0)
        self.Nbands = (len(atoms)-charge) // 2
        print("Atoms: %d Electrons: %d Bands: %d." % (N, len(atoms)-charge, self.Nbands))
        self.T_ixx = []

        # Neglect of differential overlap, coulomb matrix has just 2-indices instead of 4
        self.K_XX = np.zeros((N,N))
        self.V_ixx = []
        self.H_ixx = []
        self.rho_ixx = []

        self.rho_XX = np.zeros((N,N))
        self.converged = False

    def initialize(self):
        self.build_core_hamiltonian()
        self.build_coulomb_matrix()

        # External potential (each site has charge 1)

        Vext_XX = np.diag(np.dot(self.K_XX, -np.ones((self.N,))))
        
        self.V_ixx = self.symmetry_project_hamiltonian(Vext_XX)

        self.H_ixx = []
        for i in range(self.Ni):
            self.H_ixx.append(self.T_ixx[i] + self.V_ixx[i])

        self.Etot = None

    def build_core_hamiltonian(self):
        atoms = self.atoms
        N = len(self.atoms)
        T_XX = np.zeros((N,N)) # One basis function per atom for now
        for a1 in range(N):
            for a2 in range(N):
                if a1 != a2:
                    T_XX[a1,a2] = -3*np.exp(-atoms.get_distance(a1,a2))
                else:
                    T_XX[a1,a2] = 0.0
        self.T_ixx = self.symmetry_project_hamiltonian(T_XX)

    def symmetry_project_hamiltonian(self, H_XX):
        H_ixx = []
        for i in range(self.Ni):
            U_Xx = self.U_iXx[i]
            H_ixx.append(np.dot(U_Xx.T.conjugate(), np.dot(H_XX, U_Xx)))
        err = self.reconstruct_hamiltonian(H_ixx) - H_XX
        #assert(np.linalg.norm(err)<1e-8)
        return H_ixx

    def reconstruct_hamiltonian(self, H_ixx):
        H_XX = 0
        for i, U_Xx in enumerate(self.U_iXx):
            H_XX = H_XX + np.dot(U_Xx, np.dot(H_ixx[i], U_Xx.T.conjugate()))
        return H_XX

    def build_coulomb_matrix(self):
        atoms = self.atoms
        N = len(self.atoms)
        K_XX = self.K_XX
        for a1 in range(N):
            for a2 in range(N):
                if a1 != a2:
                    K_XX[a1,a2] = 1 / atoms.get_distance(a1,a2)
                else:
                    K_XX[a1,a2] = 3

    def update_hamiltonian(self):
        rho_X = np.diag(self.rho_XX)
        V_XX = np.diag(np.dot(self.K_XX, rho_X-1))
        self.V_ixx = self.symmetry_project_hamiltonian(V_XX)
        self.H_ixx = []
        for i in range(self.Ni):
            self.H_ixx.append(self.T_ixx[i] + self.V_ixx[i])

    def diagonalize(self):
        self.psi_ixn = []
        self.eps_in = []
        eigs = np.array([])
        for i, H_xx in enumerate(self.H_ixx):
            eps_n, psi_xn = np.linalg.eigh(H_xx)
            self.psi_ixn.append(psi_xn)
            self.eps_in.append(eps_n)
            eigs = np.concatenate((eigs, eps_n))
        Ef = np.mean(np.sort(eigs)[self.Nbands-1:self.Nbands+1])
        print("Fermi energy: %.5f" % Ef)
        self.f_in = []
        for i, H_xx in enumerate(self.H_ixx):
            self.f_in.append(2.0 * (self.eps_in[i] < Ef))
        total = 0
        for i, H_xx in enumerate(self.H_ixx):
            total += np.sum(self.f_in[i])
        print("Total electrons: %d" % total)
            
        
    def update_density_matrix(self, mix=0.05):
        # Construct the irrepwise density matrices
        rho_ixx = []
        for i, (f_n, psi_xn) in enumerate(zip(self.f_in, self.psi_ixn)):
            rho_ixx.append(np.dot(psi_xn*f_n, psi_xn.T.conjugate()))

        self.rho_XX = self.rho_XX*(1-mix) + mix*self.reconstruct_hamiltonian(rho_ixx)

    def scf_info_line(self):
        self.Etot_scf_error = np.std(self.Etot_history[-5:])
        if len(self.Etot_history)>5:
            if self.Etot_scf_error < 1e-8:
                print("Converged.")
                self.converged = True
        print("iter:%05d %.5f %.2f" % (self.scf_iteration, self.Etot, np.log10(self.Etot_scf_error)))

    def print_info(self):
        for i, name in enumerate(self.symmetry.names_i):
            print("i=%d %s:\n" % (i,name))
            for n, (eps, f) in enumerate(zip(self.eps_in[i], self.f_in[i])):
                print("%03d %.5f %.2f" % (n, eps, f))
            print()
        

    def calculate_total_energy(self):
        rho_X = np.diag(self.rho_XX)
        Ecore = np.trace(np.dot(self.rho_XX, self.reconstruct_hamiltonian(self.H_ixx)))
        E = Ecore - 0.5*np.dot(rho_X, np.dot(self.K_XX, rho_X))
        self.Etot = E
        return E

    def show_density_matrix(self):
        import matplotlib.pyplot as plt
        plt.imshow(self.rho_XX.real)
        plt.colorbar()
        plt.show()

    def run(self):
        self.initialize()
        self.scf_iteration = 0
        self.Etot_history = []
        while 1:
            self.scf_iteration += 1
            self.diagonalize()
            self.update_density_matrix()
            self.update_hamiltonian()
            self.Etot_history.append(self.calculate_total_energy())
            self.scf_info_line()
            if self.scf_iteration > 2500:
                break
            if self.converged:
                break
        self.print_info()
        self.show_density_matrix()


from ase.cluster import Octahedron
atoms = Octahedron('Cu', 4, cutoff=0)
from ase.io import write
from ase import Atoms
#atoms = Atoms('H2', positions=[[0,0,0],[2,0,0]])
calc = GSCalculator(atoms, charge=-2)
calc.run()
