import numpy as np
from scipy import spatial

# Simplest possible implementation of periodic TDDFT.
# DFT consists of one basis function per unit cell, in a single electron per atom metal like structure.
# Longitudinal TDDFT resembles electron gas, by emergence of longitudinal plasmon, mixing into electron-hole,
# except that there is as all single sole bands, the density of states is limited by a negative effective mass.

class TranslationSymmetry:
    # v is cartesian coordinate
    # c is axis index
    def __init__(self, R_cv, N_c):
        self.R_cv = R_cv
        self.iR_cv = 2*np.pi*np.linalg.inv(self.R_cv).T
        N_c = np.array(N_c, dtype=int)
        k = (np.indices(N_c)+0.5) / N_c[:,None,None,None] - 0.5
        self.k_kc = np.reshape(k, (3, np.prod(N_c))).T
        self.k_kv = np.dot(self.k_kc, self.iR_cv)
        self.w_k = np.ones(np.prod(N_c),) / np.prod(N_c)
        self.lookup = spatial.KDTree(self.k_kc)

    def get_index_by_vector(self, k_c):
        k_c = ((k_c + 0.5) % 1.0) - 0.5
        distance, index = self.lookup.query(k_c)
        if distance>1e-8:
            raise ValueError("No k-point matching k_c = [ %.5f, %.5f, %.5f ] found." % tuple(k_c))
        return index

    # Return displacements for real space unit cell
    # TODO: Should be calculated using basis set overlaps
    # Example output:
    # np.dot(
    # [[-1,-1,-1], 
    #  [-1,-1, 0],
    #  ...
    #  [1, 1, 1 ]], self.R_cv)

    def phases(self, R_Rv):
        phase_kR = np.exp(1j*np.dot(self.k_kv, R_Rv.T))
        return phase_kR

    def get_displacements(self, Rmax=1):
        N = 2*Rmax+1
        D = np.indices((N,N,N)) - Rmax
        D_Rc = np.reshape(D, (3, N**3)).T
        R_Rv = np.dot(D_Rc, self.R_cv)
        return R_Rv

    def N_k(self):
        return len(self.k_kc)

def cubical_cell(a=3.0):
    return np.diag([a,a,a])

def fcc_cell(a=3.0):
    h = a / 2
    return np.array([[ h, h, 0],
                     [ 0, h, h],
                     [ h, 0, h]])

class Basis:
    def N_M(self):
        return 1

class DFT:
    def __init__(self, atoms, basis, symmetry):
        self.atoms = atoms
        self.symmetry = symmetry
        self.H_kMM = np.zeros((symmetry.N_k(), basis.N_M(), basis.N_M()))
        self.f_kn = np.zeros((symmetry.N_k(), basis.N_M()))
        self.eps_kn = np.zeros((symmetry.N_k(), basis.N_M()))
        self.psi_knM = np.zeros((symmetry.N_k(), basis.N_M(), basis.N_M()), dtype=complex)
        self.rho_MM = np.zeros((basis.N_M(), basis.N_M()), dtype=complex)
        self.Nel = 1

    def update_hamiltonian(self):
        R_Rv = self.symmetry.get_displacements()
        H_RMM = self.calculate_real_space_hamiltonian_matrix(R_Rv)
        phase_kR = self.symmetry.phases(R_Rv)
        self.H_kMM = np.sum(phase_kR[:,:, None, None] * H_RMM[None, :,:,:], axis=1)
        self.H_kMM = (self.H_kMM + np.conjugate(np.transpose(self.H_kMM, (0,2,1))))/2

    def calculate_real_space_hamiltonian_matrix(self, R_Rv):
        L_R = np.sum(R_Rv**2, axis=1)**0.5
        H_RMM = np.zeros((len(R_Rv), 1, 1)) # Only one basis function for now
        H_RMM[:, 0,0] = -1*np.exp(-L_R/2.0)
        return H_RMM

    def calculate(self):
        self.update_hamiltonian()
        self.update_bands()
        self.update_occupations()
        self.update_density()

    def update_bands(self):
        for k in range(symmetry.N_k()):
            eps_n, psi_nM = np.linalg.eigh(self.H_kMM[k])
            self.eps_kn[k,:] = eps_n
            self.psi_knM[k,:,:] = psi_nM

    def update_density(self):
        self.rho_MM[:] = 0.0
        for k in range(symmetry.N_k()):
            self.rho_MM += np.dot(self.psi_knM[k].T.conjugate(), self.f_kn[k] * self.psi_knM[k])
        print("RHO_MM", self.rho_MM)

    def update_occupations(self):
        Nel = self.Nel
        kBT = 0.0025 # 0.04 quite smooth at 21x21x21
        EF = np.mean(self.eps_kn, axis=(0,1))
        def f(e):
            return 1 / (np.exp(e/kBT)+1)
        def dfde(e):
            # > syms e kBT
            # > diff( 1 / (exp(e/kBT)+1), e)
            #  -exp(e/kBT)/(kBT*(exp(e/kBT) + 1)^2)
            return -np.exp(e/kBT)/(kBT*(np.exp(e/kBT) + 1)**2)

        iterations = 0
        while 1:        
            e = self.eps_kn - EF
            self.f_kn = 2*f(e) * self.symmetry.w_k[:, None]
            N = np.sum(self.f_kn, axis=(0,1))
            dNde = np.sum(2*dfde(e) * self.symmetry.w_k[:, None], axis=(0,1))
            dN = N - Nel
            step = dNde**(-1) * dN
            if abs(step) > 0.01:
                step = np.sign(step)*0.01
            EF = EF + step
            iterations += 1
            print(EF, N, dN)
            if abs(dN)<1e-10:
                break
            if iterations > 100:
                break
        #f_xx = self.f_kn[:,0].reshape((21,21,21))[:,:,10]*21**3
        #for i in range(21):
        #    for j in range(21):
        #        print("%.2f" % f_xx[i,j], end=" ")
        #    print()

class TDDFT:
    def __init__(self, dft):
        self.dft = dft
        self.w_w = np.arange(7, 0, -0.01)
        self.chi0_wGG = np.zeros((len(self.w_w), 1, 1), dtype=complex) # hard coded to 1

    def calculate(self, q_qc):
        dft = self.dft
        f_kn = dft.f_kn
        eps_kn = dft.eps_kn
        eta = 0.02
        w_w = self.w_w
        symmetry = dft.symmetry
        chi_qw00 = []
        for q in range(len(q_qc)):
            print(q)
            q_c = q_qc[q]
            qmag = np.sum(np.dot(q_c, symmetry.iR_cv)**2)**0.5
            self.chi0_wGG[:] = 0.0
            for k in range(symmetry.N_k()):
                kp = symmetry.get_index_by_vector(symmetry.k_kc[k]+q_c)
                for n1 in range(1): # hard coded to 1
                    for n2 in range(1): # hard coded to 1
                        dE = eps_kn[k,n1] - eps_kn[kp, n2]
                        f = 1.0 / (w_w + dE + 1j*eta) - 1.0 / (w_w - dE + 1j*eta)
                        self.chi0_wGG[:, 0, 0] += f_kn[k, n1]*(2.0-f_kn[kp, n2])*f

            self.chi0_wGG /= np.linalg.det(symmetry.R_cv)
            # Dyson equation: chi_wGG = chi0_wGG + chi0_wGG K_GG chi_wGG
            # chi_wGG = (I-chi0_wGG K)^-1 chi0_wGG
            self.chi_wGG = 1/(1.0-self.chi0_wGG*4*np.pi/qmag**2) #*self.chi0_wGG # This is actually now roughly ~ EELS, altough it says chi
            chi_qw00.append(self.chi_wGG[:,0,0])
        chi_qw00 = np.array(chi_qw00)
        import matplotlib.pyplot as plt
        plt.imshow(np.log10(abs(chi_qw00.imag.T)), aspect='auto')
        plt.set_cmap('hot')
        plt.xticks([], [])
        plt.yticks([], [])
        plt.ylabel('$\omega$')
        plt.xlabel('$q$')
        plt.title('Longitudinal response')
        plt.savefig('electron_gas.png')
        plt.show()

N = 41
q_qc = np.array([ [q/N, 0, 0] for q in range(1,N) ])
symmetry = TranslationSymmetry(fcc_cell(5), [N,N,N])
dft = DFT(None, Basis(), symmetry)
dft.calculate()
TDDFT(dft).calculate(q_qc)

